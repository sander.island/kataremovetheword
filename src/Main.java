import java.util.ArrayList;
import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        removeLetters(new String[]{"s", "t", "r", "i", "n", "g", "w"}, "string");
        System.out.println();
        removeLetters(new String[]{"b", "b", "l", "l", "g", "n", "o", "a", "w"}, "balloon");
    }

    public static void removeLetters(String[] letters, String str) {
        ArrayList<String> charList = new ArrayList<>(); // Characters that we will keep

        for (String letter : letters) {
            if (str.contains(letter)) {
                str = str.replaceFirst(letter, ""); // Remove letter from the String, so that i won't be removed multiple times
            } else {
                charList.add(letter); // Adds the letter to the arrayList of character we will keep
            }
        }

        String[] result = new String[charList.size()];  // Create a new array with correct size
        result = charList.toArray(result);              // Copy list

        for (String letter : result) {
            System.out.print(letter);                   // Print list
        }
    }
}
